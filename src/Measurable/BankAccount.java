package Measurable;

public class BankAccount implements Measurable {
	private double balance;
	private String name ;

	public BankAccount() {
		balance = 0;
	}

	public BankAccount(double initialBalance) {
		//this.name = name;
		balance = initialBalance;
	}
	

	public void deposit(double amount) {
		balance = balance + amount;
	}

	public void withdraw(double amount) {
		balance = balance - amount;
	}

	public double getBalance() {
		return balance;
	}

	@Override
	public double getMeasure() {
		// TODO Auto-generated method stub
		return balance;
	}

}

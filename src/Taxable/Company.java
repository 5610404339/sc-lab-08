package Taxable;

public class Company implements Taxable  {
	private String name ;
	private double income ;
	private double outcome;
	
	public Company(String name,double income ,double outcome){
		this.name = name ;
		this. income  = income;
		this.outcome = outcome;
	}
	@Override
	public double getTax() {
	 double sumTax = 0;
	     sumTax = (this. income - this.outcome )*0.3;
		return sumTax;
	}
	

}

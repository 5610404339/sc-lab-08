package Taxable;

import java.util.ArrayList;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("***Separate Types *** " );
		ArrayList<Taxable> person = new ArrayList<Taxable>();
		person.add(new Person("par", 10000));
		person.add(new Person("milo", 104000));
		person.add(new Person("rihana", 500000));
        
		double sum = TaxCalculator.sum(person);
		System.out.println("Summation of Tax in persons : " + sum);

		ArrayList<Taxable> companies = new ArrayList<Taxable>();
		companies.add(new Company("CP Consumer", 1000000, 800000));
		companies.add(new Company("The Mall Group", 1200000, 345600));
		companies.add(new Company("Sony", 3600000, 200000));

		double sum1 = TaxCalculator.sum(companies);
		System.out.println("Summation of Tax in companies : " + sum1);

		ArrayList<Taxable> pr = new ArrayList<Taxable>();
		pr.add(new Product("Kit Kat", 200));
		pr.add(new Product("Farm House", 250));
		pr.add(new Product("Harrod Bag", 2800));

		double sum2 = TaxCalculator.sum(pr);
		System.out.println("Summation of Tax in products : " + sum2);
		
		System.out.println("***Whole Types *** " );
		ArrayList<Taxable> whole = new ArrayList<Taxable>();
		whole.add(new Person("par", 10000));
		whole.add(new Person("milo", 104000));
		whole.add(new Person("rihana", 360000));
		whole.add(new Company("CP Consumer",1000000, 800000));
		whole.add(new Company("The Mall Group",  1200000, 345600));
		whole.add(new Company("Sony", 3600000, 200000));
		whole.add(new Product("Kit Kat", 289));
		whole.add(new Product("Farm House", 25));
		whole.add(new Product("Harrod Bag", 2450));
		double sum_all = TaxCalculator.sum(whole);
		System.out.println("Summation of whole types : " + sum_all);

	}

}
